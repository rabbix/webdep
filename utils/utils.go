package utils

import (
	"fmt"
	"os"
	"runtime/debug"
	"strconv"
)

func GetEnvOrDefault(key, defaultValue string) string {
	value := os.Getenv(key)
	if value != "" {
		return value
	}
	return defaultValue
}

func PanicAtoi(in, message string) int {
	r, err := strconv.Atoi(in)
	if err != nil {
		panic(message + ": " + err.Error())
	}
	return r
}

// someParam := "abc"
// utils.SafeGo(func() {
//	mFunc(someParam)
//})
func SafeGo(f func()) {
	go func() {
		defer func() {
			if err := recover(); err != nil {
				stack := debug.Stack()
				fmt.Printf("RECOVERED FROM UNHANDLED PANIC: %v\nSTACK: %s", err, stack)
			}
		}()
		f()
	}()
}
