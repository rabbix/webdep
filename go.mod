module gitee.com/rabbix/webdep

go 1.16

require (
	github.com/casbin/casbin/v2 v2.31.10
	github.com/casbin/gorm-adapter/v3 v3.3.2
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gin-gonic/gin v1.7.2
	github.com/go-playground/assert/v2 v2.0.1
	github.com/go-redis/redis/v8 v8.11.0
	github.com/go-spring/spring-core v1.0.5
	github.com/satori/go.uuid v1.2.0
	github.com/sirupsen/logrus v1.4.2
	golang.org/x/sync v0.0.0-20201020160332-67f06af15bc9
	golang.org/x/sys v0.0.0-20210525143221-35b2ab0089ea // indirect
	gorm.io/driver/mysql v1.1.1
	gorm.io/driver/sqlserver v1.0.7
	gorm.io/gorm v1.21.11
)
