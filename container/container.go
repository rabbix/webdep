package container

import (
	"context"
	SpringCore "github.com/go-spring/spring-core"
)

var c = SpringCore.NewDefaultSpringContext()

func Context() context.Context {
	return c.Context()
}

func GetProfile() string {
	return c.GetProfile()
}

func SetProfile(profile string) {
	c.SetProfile(profile)
}

func AllAccess() bool {
	return c.AllAccess()
}

func SetAllAccess(allAccess bool) {
	c.SetAllAccess(allAccess)
}

func RegisterBeanDefinition(bd *SpringCore.BeanDefinition) *SpringCore.BeanDefinition {
	return c.RegisterBeanDefinition(bd)
}

func RegisterBean(bean interface{}) *SpringCore.BeanDefinition {
	return c.RegisterBean(bean)
}

func RegisterNameBean(name string, bean interface{}) *SpringCore.BeanDefinition {
	return c.RegisterNameBean(name, bean)
}

func RegisterBeanFn(fn interface{}, tags ...string) *SpringCore.BeanDefinition {
	return c.RegisterBeanFn(fn, tags...)
}

func RegisterNameBeanFn(name string, fn interface{}, tags ...string) *SpringCore.BeanDefinition {
	return c.RegisterNameBeanFn(name, fn, tags...)
}

func RegisterMethodBean(selector SpringCore.BeanSelector, method string, tags ...string) *SpringCore.BeanDefinition {
	return c.RegisterMethodBean(selector, method, tags...)
}

func RegisterNameMethodBean(name string, selector SpringCore.BeanSelector, method string, tags ...string) *SpringCore.BeanDefinition {
	return c.RegisterNameMethodBean(name, selector, method, tags...)
}

func RegisterMethodBeanFn(method interface{}, tags ...string) *SpringCore.BeanDefinition {
	return c.RegisterMethodBeanFn(method, tags...)
}

func RegisterNameMethodBeanFn(name string, method interface{}, tags ...string) *SpringCore.BeanDefinition {
	return c.RegisterNameMethodBeanFn(name, method, tags...)
}

func GetBean(i interface{}, selector ...SpringCore.BeanSelector) bool {
	return c.GetBean(i, selector...)
}

func FindBean(selector SpringCore.BeanSelector) (*SpringCore.BeanDefinition, bool) {
	return c.FindBean(selector)
}

func CollectBeans(i interface{}, selectors ...SpringCore.BeanSelector) bool {
	return c.CollectBeans(i, selectors...)
}

func AutoWireBeans() {
	c.AutoWireBeans()
}

func WireBean(i interface{}) {
	c.WireBean(i)
}

func GetBeanDefinitions() []*SpringCore.BeanDefinition {
	return c.GetBeanDefinitions()
}

func Close(beforeDestroy ...func()) {
	c.Close(beforeDestroy...)
}

func Run(fn interface{}, tags ...string) *SpringCore.Runner {
	return c.Run(fn, tags...)
}

func RunNow(fn interface{}, tags ...string) error {
	return c.RunNow(fn, tags...)
}

func Config(fn interface{}, tags ...string) *SpringCore.Configer {
	return c.Config(fn, tags...)
}

func ConfigWithName(name string, fn interface{}, tags ...string) *SpringCore.Configer {
	return c.ConfigWithName(name, fn, tags...)
}

func SafeGoroutine(fn SpringCore.GoFunc) {
	c.SafeGoroutine(fn)
}
