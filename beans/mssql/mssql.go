package mssql

import (
	"gitee.com/rabbix/webdep/container"
	"gitee.com/rabbix/webdep/utils"
	"gorm.io/driver/sqlserver"
	"gorm.io/gorm"
	"strconv"
)

func init() {
	container.RegisterBeanFn(func() *gorm.DB {
		host := utils.GetEnvOrDefault("M_MSSQL_HOST", "127.0.0.1")
		port := utils.GetEnvOrDefault("M_MSSQL_PORT", "1433")
		user := utils.GetEnvOrDefault("M_MSSQL_USER", "sa")
		password := utils.GetEnvOrDefault("M_MSSQL_PASSWORD", "Microsoft123456")
		database := utils.GetEnvOrDefault("M_MSSQL_DATABASE", "test")
		maxIdleConns := utils.GetEnvOrDefault("M_MSSQL_MAX_IDLE_CONNS", "10")
		maxOpenConns := utils.GetEnvOrDefault("M_MSSQL_MAX_OPEN_CONNS", "10")

		dsn := "sqlserver://" + user + ":" + password + "@" + host + ":" + port + "?database=" + database

		db, err := gorm.Open(sqlserver.Open(dsn), &gorm.Config{})
		if err != nil {
			panic("连接数据库失败: " + err.Error())
		}
		rawDB, _ := db.DB()
		if t, err := strconv.Atoi(maxIdleConns); err != nil {
			panic("M_MSSQL_MAX_IDLE_CONNS必须是整数, " + err.Error())
		} else {
			rawDB.SetMaxIdleConns(t)
		}

		if t, err := strconv.Atoi(maxOpenConns); err != nil {
			panic("M_MSSQL_MAX_OPEN_CONNS必须是整数, " + err.Error())
		} else {
			rawDB.SetMaxOpenConns(t)
		}
		return db
	})
}
