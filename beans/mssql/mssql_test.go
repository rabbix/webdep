package mssql

import (
	"fmt"
	"gitee.com/rabbix/webdep/container"
	"gorm.io/gorm"
	"os"
	"testing"
)

type someService struct {
	DBConn *gorm.DB `autowire:""`
}

func Test_mysql(t *testing.T) {
	os.Setenv("M_MSSQL_HOST", "10.0.0.2")
	os.Setenv("M_MSSQL_DATABASE", "test1")

	obj := &someService{}
	container.RegisterBean(obj)

	container.AutoWireBeans()

	rows, _ := obj.DBConn.Raw("select top 1 * from sbtest1").Rows()
	defer rows.Close()

	cols, _ := rows.Columns()
	values := make([][]byte, len(cols))
	scans := make([]interface{}, len(cols))
	for i := range values {
		scans[i] = &values[i]
	}
	results := make(map[int]map[string]string)
	i := 0
	for rows.Next() {
		if err := rows.Scan(scans...); err != nil {
			fmt.Println(err)
			return
		}
		row := make(map[string]string) //每行数据
		for k, v := range values {     //每行数据是放在values里面，现在把它挪到row里
			key := cols[k]
			row[key] = string(v)
		}
		results[i] = row //装入结果集中
		i++
	}
	//查询出来的数组
	for k, v := range results {
		fmt.Println(k, v)
	}
}
