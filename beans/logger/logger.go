package logger

import (
	"gitee.com/rabbix/webdep/container"
	"gitee.com/rabbix/webdep/utils"
	"github.com/sirupsen/logrus"
	"os"
)

func init() {
	container.RegisterBeanFn(func() *logrus.Logger {
		levels := map[string]logrus.Level{
			"PANIC": logrus.PanicLevel,
			"Fatal": logrus.FatalLevel,
			"ERROR": logrus.ErrorLevel,
			"WARN":  logrus.WarnLevel,
			"INFO":  logrus.InfoLevel,
			"DEBUG": logrus.DebugLevel,
			"TRACE": logrus.TraceLevel,
		}

		formatters := map[string]logrus.Formatter{
			"JSON": new(logrus.JSONFormatter),
			"TEXT": new(logrus.TextFormatter),
		}

		logFile := utils.GetEnvOrDefault("M_LOG_FILE", "")
		logLevel := utils.GetEnvOrDefault("M_LOG_LEVEL", "INFO")
		logForMatter := utils.GetEnvOrDefault("M_LOG_FORMATTER", "JSON")
		logCaller := utils.GetEnvOrDefault("M_LOG_CALLER", "TRUE")

		logger := logrus.New()
		if logFile == "" {
			logger.SetOutput(os.Stdout)
		} else {
			file, err := os.OpenFile(logFile, os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0666)
			if err != nil {
				panic("不能打开日志文件: " + err.Error())
			}
			logger.SetOutput(file)
		}

		if level, ok := levels[logLevel]; ok {
			logger.SetLevel(level)
		} else {
			logger.SetLevel(logrus.InfoLevel)
		}

		if logCaller == "TRUE" {
			logger.ReportCaller = true
		}

		if formatter, ok := formatters[logForMatter]; ok {
			logger.SetFormatter(formatter)
		} else {
			logger.SetFormatter(&logrus.JSONFormatter{})
		}

		return logger
	})
}
