package logger

import (
	"gitee.com/rabbix/webdep/container"
	"github.com/sirupsen/logrus"
	"testing"
)

type logTest struct {
	Logger *logrus.Logger `autowire:""`
}

func Test_logger(t *testing.T) {
	test := new(logTest)
	container.RegisterBean(test)
	container.AutoWireBeans()

	test.Logger.WithFields(logrus.Fields{
		"f1": "v1",
		"f2": "v2",
	}).Info("测试信息")
}
