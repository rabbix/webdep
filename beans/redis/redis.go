package redis

import (
	"gitee.com/rabbix/webdep/container"
	"gitee.com/rabbix/webdep/utils"
	"github.com/go-redis/redis/v8"
)

func init() {
	container.RegisterBeanFn(func() *redis.Client {
		host := utils.GetEnvOrDefault("M_REDIS_HOST", "127.0.0.1")
		port := utils.GetEnvOrDefault("M_REDIS_PORT", "6379")
		password := utils.GetEnvOrDefault("M_REDIS_PASSWORD", "123456")
		db := utils.PanicAtoi(utils.GetEnvOrDefault("M_REDIS_DB", "0"), "M_REDIS_DB 必须是整数")
		poolSize := utils.PanicAtoi(utils.GetEnvOrDefault("M_REDIS_POOL_SIZE", "10"), "M_REDIS_POOL_SIZE 必须是整数")
		minIdleConns := utils.PanicAtoi(utils.GetEnvOrDefault("M_REDIS_MIN_IDLE_CONNS", "10"), "M_REDIS_MIN_IDLE_CONNS 必须是整数")

		addr := host + ":" + port

		return redis.NewClient(&redis.Options{
			Addr:         addr,
			Password:     password, // no password set
			DB:           db,       // use default DB
			PoolSize:     poolSize,
			MinIdleConns: minIdleConns,
		})
	})
}
