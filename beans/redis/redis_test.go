package redis

import (
	"context"
	"gitee.com/rabbix/webdep/container"
	"github.com/go-redis/redis/v8"
	"os"
	"testing"
)

type someService struct {
	RedisClient *redis.Client `autowire:""`
}

func Test_redis(t *testing.T) {
	os.Setenv("M_REDIS_HOST", "10.0.0.3")

	obj := &someService{}
	container.RegisterBean(obj)

	container.AutoWireBeans()

	ctx := context.Background()
	r, err := obj.RedisClient.Get(ctx, "test").Result()
	if err != nil {
		t.Error(err.Error())
	}
	println("获取到的值: " + r)
}
