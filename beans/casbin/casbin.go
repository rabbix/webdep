package casbin

import (
	"gitee.com/rabbix/webdep/container"
	"github.com/casbin/casbin/v2"
	"github.com/casbin/casbin/v2/model"
	"github.com/casbin/casbin/v2/util"
	"github.com/casbin/gorm-adapter/v3"
	"gorm.io/gorm"
)

func init() {
	container.RegisterBeanFn(func(db *gorm.DB) *casbin.Enforcer {
		adapter, err := gormadapter.NewAdapterByDB(db)
		if err != nil {
			panic("初始化casbin失败: " + err.Error())
		}
		m := model.NewModel()
		m.AddDef("r", "r", "sub, obj, act")
		m.AddDef("p", "p", "sub, obj, act")
		m.AddDef("g", "g", "_, _")
		m.AddDef("e", "e", "some(where (p.eft == allow))")
		m.AddDef("m", "m", "g(r.sub, p.sub) && KeyMatch2Func(r.obj,p.obj) && r.act == p.act")

		enforcer, err := casbin.NewEnforcer(m, adapter)
		if err != nil {
			panic("初始化casbin失败: " + err.Error())
		}
		enforcer.AddFunction("KeyMatch2Func", util.KeyMatch2Func)
		if err := enforcer.LoadPolicy(); err != nil {
			panic("casbin加载策略失败： " + err.Error())
		}
		return enforcer
	})
}
