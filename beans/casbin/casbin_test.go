package casbin

import (
	_ "gitee.com/rabbix/webdep/beans/mysql"
	"gitee.com/rabbix/webdep/container"
	"github.com/casbin/casbin/v2"
	"os"
	"testing"
)

func Test_casbin(t *testing.T) {
	os.Setenv("M_MYSQL_HOST", "10.0.0.2")
	os.Setenv("M_MYSQL_DATABASE", "test1")

	container.AutoWireBeans()

	var e *casbin.Enforcer
	container.GetBean(&e)

	e.AddPolicy("role:admin", "/admin/login", "POST")
	e.AddPolicy("role:admin", "/admin/login", "POST")
	e.AddRoleForUser("bob", "role::admin")

	if ok, err := e.Enforce("alis", "/admin/login", "POST"); err == nil && ok {
		println("ok")
	} else {
		t.Error(err)
	}

	if ok, err := e.Enforce("bob", "/admin/login", "POST"); err == nil && ok {
		println("ok")
	} else {
		t.Error(err)
	}
}
