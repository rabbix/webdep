package mysql

import (
	"gitee.com/rabbix/webdep/container"
	"gitee.com/rabbix/webdep/utils"
	msql "gorm.io/driver/mysql"
	"gorm.io/gorm"
	"os"
	"strconv"
)

func init() {
	container.RegisterBeanFn(func() *gorm.DB {
		host := utils.GetEnvOrDefault("M_MYSQL_HOST", "127.0.0.1")
		port := utils.GetEnvOrDefault("M_MYSQL_PORT", "3306")
		user := utils.GetEnvOrDefault("M_MYSQL_USER", "root")
		password := utils.GetEnvOrDefault("M_MYSQL_PASSWORD", "123456")
		database := utils.GetEnvOrDefault("M_MYSQL_DATABASE", "test")
		charset := utils.GetEnvOrDefault("M_MYSQL_CHARSET", "utf8mb4")
		maxIdleConns := utils.GetEnvOrDefault("M_MYSQL_MAX_IDLE_CONNS", "10")
		maxOpenConns := utils.GetEnvOrDefault("M_MYSQL_MAX_OPEN_CONNS", "10")

		extraParams := os.Getenv("M_MYSQL_EXTRA_PARAMS")

		dsn := user + ":" + password + "@tcp(" + host + ":" + port + ")/" + database + "?charset=" + charset
		if extraParams != "" {
			dsn += "&" + extraParams
		}

		db, err := gorm.Open(msql.Open(dsn), &gorm.Config{})
		if err != nil {
			panic("连接数据库失败: " + err.Error())
		}
		rawDB, _ := db.DB()
		if t, err := strconv.Atoi(maxIdleConns); err != nil {
			panic("M_MYSQL_MAX_IDLE_CONNS必须是整数, " + err.Error())
		} else {
			rawDB.SetMaxIdleConns(t)
		}

		if t, err := strconv.Atoi(maxOpenConns); err != nil {
			panic("M_MYSQL_MAX_OPEN_CONNS必须是整数, " + err.Error())
		} else {
			rawDB.SetMaxOpenConns(t)
		}
		return db
	})
}
