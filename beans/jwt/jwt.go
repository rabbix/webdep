package jwt

import (
	"gitee.com/rabbix/webdep/container"
	"gitee.com/rabbix/webdep/models/common/response"
	jwtService "gitee.com/rabbix/webdep/services/jwt"
	"gitee.com/rabbix/webdep/utils"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"strconv"
	"time"
)

func init() {
	SIGNING_KEY := utils.GetEnvOrDefault("M_JWT_KEY", "!@#$%^&*()")
	EXPIRE := utils.GetEnvOrDefault("M_JET_EXPIRE", "604800")

	container.RegisterNameBeanFn("jwt", func(logger *logrus.Logger) gin.HandlerFunc {
		return func(c *gin.Context) {
			token := c.Request.Header.Get("x-token")
			if token == "" {
				response.FailWithDetailed(gin.H{"reload": true}, "未登录或非法访问", c)
				c.Abort()
				return
			}
			claims, err := jwtService.ParseToken(token, SIGNING_KEY)
			if err != nil {
				if err == jwtService.TokenExpired {
					response.FailWithDetailed(gin.H{"reload": true}, "授权已过期", c)
					c.Abort()
					return
				}
				response.FailWithDetailed(gin.H{"reload": true}, err.Error(), c)
				c.Abort()
				return
			}
			if claims.ExpiresAt-time.Now().Unix() < claims.BufferTime {
				claims.ExpiresAt = time.Now().Unix() + int64(utils.PanicAtoi(EXPIRE, "error: jwt expire atoi"))
				newToken, _ := jwtService.CreateTokenByOldToken(token, SIGNING_KEY, *claims)
				newClaims, _ := jwtService.ParseToken(newToken, SIGNING_KEY)
				c.Header("new-token", newToken)
				c.Header("new-expires-at", strconv.FormatInt(newClaims.ExpiresAt, 10))
			}
			c.Set("claims", claims)
			c.Next()
		}
	})
}
