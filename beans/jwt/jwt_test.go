package jwt

import (
	_ "gitee.com/rabbix/webdep/beans/logger"
	"gitee.com/rabbix/webdep/container"
	"github.com/gin-gonic/gin"
	"github.com/go-playground/assert/v2"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
)

type header struct {
	Key   string
	Value string
}

func performRequest(r http.Handler, method, path string, headers ...header) *httptest.ResponseRecorder {
	req := httptest.NewRequest(method, path, nil)
	for _, h := range headers {
		req.Header.Add(h.Key, h.Value)
	}
	w := httptest.NewRecorder()
	r.ServeHTTP(w, req)
	return w
}

func TestJWT(t *testing.T) {
	os.Setenv("M_JWT_KEY", "!@#$%^&*()")
	os.Setenv("M_JET_EXPIRE", "604800")
	container.AutoWireBeans()
	var jwt gin.HandlerFunc

	container.GetBean(&jwt, "jwt")

	if jwt == nil {
		t.Error("jwt is nil")
		return
	}

	router := gin.New()

	router.Use(jwt).
		GET("/hello", func(c *gin.Context) {
			c.String(http.StatusOK, "sup2")
		})

	w := performRequest(router, http.MethodGet, "/hello")
	t.Log(w.Body)

	assert.Equal(t, http.StatusOK, w.Code)
}
